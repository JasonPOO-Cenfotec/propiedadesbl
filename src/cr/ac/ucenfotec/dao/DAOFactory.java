package cr.ac.ucenfotec.dao;

import cr.ac.ucenfotec.bl.entites.inquilino.IInquilino;
import cr.ac.ucenfotec.bl.entites.propiedad.IPropiedad;

public abstract class DAOFactory {

    private static final int MYSQL =1;

    public static DAOFactory getDAOFactory(int repositorio){
       switch (repositorio){
           case MYSQL:
                return new MySQLDaoFactory();
           default:
                return null;
       }
    }

    public abstract IInquilino getInquilinoDao();
    public abstract IPropiedad getPropiedadDao();

}
