package cr.ac.ucenfotec.dao;

import cr.ac.ucenfotec.bl.entites.inquilino.IInquilino;
import cr.ac.ucenfotec.bl.entites.inquilino.MySQLInquilinoImpl;
import cr.ac.ucenfotec.bl.entites.propiedad.IPropiedad;
import cr.ac.ucenfotec.bl.entites.propiedad.MySQLPropiedadImpl;

public class MySQLDaoFactory extends DAOFactory{

    public IInquilino getInquilinoDao() {
        return new MySQLInquilinoImpl();
    }

    public IPropiedad getPropiedadDao() {
        return new MySQLPropiedadImpl();
    }
}
