package cr.ac.ucenfotec.dl;

import cr.ac.ucenfotec.util.Utils;

public class Conector {

    private static AccesoBD  coneccionBD = null;

    public static  AccesoBD getConnector() throws Exception {
        String[] infoBD = Utils.getProperties();
        String URL = infoBD[0]+"//"+infoBD[1]+"/"+infoBD[2]+infoBD[3];
        String user = infoBD[4];
        String password =infoBD[5];

        if(coneccionBD == null){
            coneccionBD = new AccesoBD(URL,user,password);
        }
        return coneccionBD;
    }
}
