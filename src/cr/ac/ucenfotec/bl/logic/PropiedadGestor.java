package cr.ac.ucenfotec.bl.logic;

import cr.ac.ucenfotec.bl.entites.inquilino.Inquilino;
import cr.ac.ucenfotec.bl.entites.propiedad.IPropiedad;
import cr.ac.ucenfotec.bl.entites.propiedad.MySQLPropiedadImpl;
import cr.ac.ucenfotec.bl.entites.propiedad.Propiedad;
import cr.ac.ucenfotec.dao.DAOFactory;

import java.util.ArrayList;

public class PropiedadGestor {

    private DAOFactory factory;
    private IPropiedad datos;

    public PropiedadGestor(){
        factory = DAOFactory.getDAOFactory(2);
        datos = factory.getPropiedadDao();
    }

    public String registrarPropiedad(int codigo, String nombre, int cuartos,String provincia, double costo, boolean estado) throws Exception{
        Propiedad propiedad = new Propiedad(codigo,nombre,cuartos,provincia,costo,estado);
        return datos.registrarPropiedad(propiedad);
    }

    public ArrayList<Propiedad> listarPropiedades() throws Exception {
       return datos.listarPropiedades();
    }

    public String alquilarPropiedad(int cedula,int codigo)throws Exception{
        // 1. Buscar el inquilo con la cedula
        Inquilino inquilino = new Inquilino();
        inquilino.setCedula(cedula);

        // 2. Buscar la propiedad con el codigo
        Propiedad propiedad = new Propiedad();
        propiedad.setCodigo(codigo);

        // 3. Llamar al método de alquilar propiedad
        return datos.alquilarPropiedad(inquilino,propiedad);
    }

}


















