package cr.ac.ucenfotec.bl.entites.inquilino;

public interface IInquilino {
    String registrarInquilino(Inquilino inquilino) throws Exception;
}
