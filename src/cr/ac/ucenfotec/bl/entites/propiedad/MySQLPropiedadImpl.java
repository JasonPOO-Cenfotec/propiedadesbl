package cr.ac.ucenfotec.bl.entites.propiedad;

import cr.ac.ucenfotec.bl.entites.inquilino.Inquilino;
import cr.ac.ucenfotec.dl.Conector;

import java.sql.ResultSet;
import java.util.ArrayList;

public class MySQLPropiedadImpl implements IPropiedad{

    private String sqlQuery="";

    public String registrarPropiedad(Propiedad propiedad) throws Exception {
        sqlQuery="INSERT INTO.....";
        Conector.getConnector().ejecutarSQL(sqlQuery);

        return "Propiedad registrada correctamente!";
    }


    public String modificarPropiedad(Propiedad propiedad) throws Exception {
        sqlQuery="UPDATE.....";
        Conector.getConnector().ejecutarSQL(sqlQuery);

        return "Propiedad modificada correctamente!";
    }


    public String eliminarPropiedad(int codigo) throws Exception {
        sqlQuery="DELETE.....";
        Conector.getConnector().ejecutarSQL(sqlQuery);

        return "Propiedad eliminada correctamente!";
    }


    public ArrayList<Propiedad> listarPropiedades() throws Exception {

        ArrayList<Propiedad> listaPropiedades = new ArrayList<>();
        sqlQuery="SELECT P.CODIGO, P.NOMBRE NOMBRE_PROPIEDAD, P.CANTIDAD,P.PROVINCIA,P.COSTO,P.ESTADO, " +
                "I.CEDULA CEDULAINQILINO, I.NOMBRE NOMBRE_INQUILINO, I.PROFESION, I.EDAD " +
                "FROM PROPIEDAD P LEFT JOIN INQUILINO I " +
                "ON P.CEDULAINQUILINO = I.CEDULA";
        ResultSet rs = Conector.getConnector().ejecutarQuery(sqlQuery);

        while(rs.next()){
            Propiedad propiedad = new Propiedad();
            propiedad.setCodigo(rs.getInt("CODIGO"));
            propiedad.setNombre(rs.getString("NOMBRE_PROPIEDAD"));
            propiedad.setCuartos(rs.getInt("CANTIDAD"));
            propiedad.setProvincia(rs.getString("PROVINCIA"));
            propiedad.setCosto(rs.getDouble("COSTO"));
            propiedad.setEstado(rs.getBoolean("ESTADO"));

            if(rs.getInt("CEDULAINQILINO")> 0){
                Inquilino inquilino = new Inquilino();
                inquilino.setCedula(rs.getInt("CEDULAINQILINO"));
                inquilino.setNombre(rs.getString("NOMBRE_INQUILINO"));
                inquilino.setProfesion(rs.getString("PROFESION"));
                inquilino.setEdad(rs.getInt("EDAD"));

                propiedad.setInquilino(inquilino);
            }
            listaPropiedades.add(propiedad);
        }
        return listaPropiedades;
    }

    public double getMontoTotalAlquiler() throws Exception {
        return 0;
    }


    public String alquilarPropiedad(Inquilino inquilino, Propiedad propiedad) throws Exception {
        sqlQuery="UPDATE PROPIEDAD SET ESTADO = 1, CEDULAINQUILINO="+inquilino.getCedula()+" WHERE CODIGO="+propiedad.getCodigo()+"";
        Conector.getConnector().ejecutarSQL(sqlQuery);
        return "La propiedad codigo " + propiedad.getCodigo() + " fue alquilada por "+inquilino.getCedula();
    }
}




































