package cr.ac.ucenfotec.bl.entites.propiedad;

import cr.ac.ucenfotec.bl.entites.inquilino.Inquilino;

public class Propiedad {

    private int codigo;
    private String nombre;
    private int cuartos;
    private String provincia;
    private double costo;
    private boolean estado;
    private Inquilino inquilino;

    public Propiedad() {
    }

    public Propiedad(int codigo, String nombre, int cuartos,String provincia, double costo, boolean estado) {
        this.codigo = codigo;
        this.nombre = nombre;
        this.cuartos = cuartos;
        this.provincia = provincia;
        this.costo = costo;
        this.estado = estado;
    }

    public Propiedad(String nombre, int cuartos,String provincia, double costo) {
        this.nombre = nombre;
        this.cuartos = cuartos;
        this.provincia = provincia;
        this.costo = costo;
    }

    public int getCodigo() {
        return codigo;
    }

    public void setCodigo(int codigo) {
        this.codigo = codigo;
    }

    public String getNombre() {
        return nombre;
    }

    public void setNombre(String nombre) {
        this.nombre = nombre;
    }

    public int getCuartos() {
        return cuartos;
    }

    public void setCuartos(int cuartos) {
        this.cuartos = cuartos;
    }

    public String getProvincia() {
        return provincia;
    }

    public void setProvincia(String provincia) {
        this.provincia = provincia;
    }

    public double getCosto() {
        return costo;
    }

    public void setCosto(double costo) {
        this.costo = costo;
    }

    public boolean isEstado() {
        return estado;
    }

    public void setEstado(boolean estado) {
        this.estado = estado;
    }

    public Inquilino getInquilino() {
        return inquilino;
    }

    public void setInquilino(Inquilino inquilino) {
        this.inquilino = inquilino;
    }

    public String toString() {
        return "Propiedad{" +
                "codigo=" + codigo +
                ", nombre='" + nombre + '\'' +
                ", cuartos=" + cuartos +
                ", provincia=" + provincia +
                ", costo=" + costo +
                ", estado=" + estado +
                ", inquilino=" + inquilino +
                '}';
    }

    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;
        Propiedad propiedad = (Propiedad) o;
        return codigo == propiedad.codigo;
    }
}
