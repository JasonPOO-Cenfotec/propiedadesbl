package cr.ac.ucenfotec.bl.entites.propiedad;

import cr.ac.ucenfotec.bl.entites.inquilino.Inquilino;

import java.util.ArrayList;

public interface IPropiedad {
     String registrarPropiedad(Propiedad propiedad) throws Exception;
     String modificarPropiedad(Propiedad propiedad) throws Exception;
     String eliminarPropiedad(int codigo) throws Exception;
     ArrayList<Propiedad> listarPropiedades()throws Exception;
     double getMontoTotalAlquiler() throws Exception;
     String alquilarPropiedad(Inquilino inquilino, Propiedad propiedad) throws Exception;
}
